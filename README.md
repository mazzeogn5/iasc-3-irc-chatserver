# IRC

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add tp3_irc to your list of dependencies in `mix.exs`:

        def deps do
          [{:tp3_irc, "~> 0.0.1"}]
        end

  2. Ensure tp3_irc is started before your application:

        def application do
          [applications: [:tp3_irc]]
        end

