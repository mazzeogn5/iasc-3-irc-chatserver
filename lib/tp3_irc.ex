defmodule Server do

	def new do
		spawn(fn -> loop(Map.new(), 0, Map.new(), Map.new()) end)
	end

	def loop(messages, last_msg_id, groups, clients) do
		receive do
			{:connect, sender} -> 
				send sender, {:connect_ack, self()}
				loop(messages, last_msg_id, groups, Map.put(clients, sender, MapSet.new))	

			{:deliver_msg, origin, dest, msg} ->
				if 	MapSet.member?(Map.get(clients, dest), origin) do
					IO.puts("Server  -  #{inspect dest} is silenced by #{inspect origin}")
				else
					send dest, {:receive_msg, origin, msg, last_msg_id}	
				end

				loop(Map.put(messages, last_msg_id, {origin, dest, msg}), last_msg_id+1, groups, clients)	

			{:msg_delivered, to, from, msg_id} ->
				send from, {:msg_delivered, to, msg_id}
				loop(messages, last_msg_id, groups, clients)

			{:msg_read, client, msg_id} ->
				{origin, dest, msg} = Map.get(messages, msg_id)
				send origin, {:msg_read_notify, msg_id, client}
				loop(messages, last_msg_id, groups, clients)


			{:started_writing_notify, origin, dest} ->
				send dest, {:started_writing_notify, origin}
				loop(messages, last_msg_id, groups, clients)	

			{:silence_client, client, to_be_silence} ->
				IO.puts("Server  -  #{inspect client} is silenced #{inspect to_be_silence}")
				loop(messages, last_msg_id, groups, Map.put(clients, client, MapSet.put(Map.get(clients, client), to_be_silence)))		

			{:unsilence_client, client, to_be_unsilence} ->
				IO.puts("Server  -  #{inspect client} is unsilenced #{inspect to_be_unsilence}")
				loop(messages, last_msg_id, groups, Map.put(clients, client, MapSet.delete(Map.get(clients, client), to_be_unsilence)))	

			{:create_group, client, group_name} ->
				loop(messages, last_msg_id, Map.put(groups, group_name, MapSet.put(MapSet.new(), client)), clients)		

			{:join_client_to_group, client, group_name} ->
				loop(messages, last_msg_id, Map.put(groups, group_name, MapSet.put(Map.get(groups, group_name), client)), clients)		

			{:deliver_msg_to_group, client, group_name, msg} ->
				Enum.each(
					Map.get(groups, group_name), 
					fn group_member -> 
						if 	MapSet.member?(Map.get(clients, group_member), client) do
							IO.puts("Server  -  #{inspect group_member} is silenced by #{inspect client}")
						else
							send group_member, {:receive_msg, group_name, client, msg, last_msg_id}	
						end
					end)
				loop(messages, last_msg_id+1, groups, clients)

			{:group_members, group_name} ->
				group_members = Map.get(groups, group_name)
				IO.puts("Server  -  #{inspect group_members}")
				loop(messages, last_msg_id, groups, clients)
		end
	end

	def group_members(server,group_name) do
		send server, {:group_members, group_name}
	end
end

defmodule Client do

	def new(server) do 
		spawn(fn -> init(Map.new(), server) end)
	end 

	def init(messages, server) do
		send server, {:connect, self}
		receive do 
			{:connect_ack, ^server} -> 
				IO.puts "#{inspect self} is now connected to #{inspect server}"
				loop(messages, server)
		end 
	end 
	

	def loop(messages, server) do
		receive do
			{:send_msg, dest, msg} ->
				send server, {:deliver_msg, self, dest, msg}
				loop(messages, server)

			{:receive_msg, origin, msg, msg_id} ->
				IO.puts "#{inspect self}  -  Received new message: #{msg}"
				send server, {:msg_delivered, self, origin, msg_id}
				loop(Map.put(messages, origin, Map.get(messages, origin, []) ++ [{msg_id, msg, false}]), server)

			{:read_msg, origin} ->
				IO.puts("#{inspect self}  -  Reading msgs from #{inspect origin}")
				Enum.each(
					Map.get(messages, origin, []),
					fn {msg_id, msg, read} -> 
						if !read do
							IO.puts("#{inspect self}  -  Message #{inspect msg_id} from #{inspect origin}: #{inspect msg}")
							send server, {:msg_read, self, msg_id}
						end
					end)

				msgs = Enum.map(
						Map.get(messages,origin, []),
						fn {msg_id, msg, read} -> 
							if(!read) do
								{msg_id, msg, true}
							else
								{msg_id, msg, read}
							end
						end
					)	

				loop(Map.put(messages, origin, 	msgs) , server)

			{:msg_read_notify, msg_id, client} ->
				IO.puts("#{inspect client} has read your msg: #{inspect msg_id}")
				loop(messages, server)

			{:receive_msg, group, origin, msg, msg_id} ->
				IO.puts "#{inspect self} received new message from group #{inspect group} - #{inspect origin}: #{msg}"
				send server, {:msg_delivered, self, origin, msg_id}
				loop(messages, server)

			{:msg_delivered, to, msg_id} ->
				loop(messages, server)

			{:started_writing, dest} ->
				send server, {:started_writing_notify, self, dest}
				loop(messages, server)

			{:started_writing_notify, origin} ->
				 IO.puts "#{inspect origin} is writing..." 
				loop(messages, server)

			{:silence, to_be_silence} ->
				send server, {:silence_client, self, to_be_silence}
				loop(messages, server)

			{:unsilence, to_be_unsilence} ->
				send server, {:unsilence_client, self, to_be_unsilence}
				loop(messages, server)

			{:create_group, group_name} ->
				send server, {:create_group, self, group_name}
				loop(messages, server)

			{:join_group, group_name} ->
				send server, {:join_client_to_group, self, group_name}
				loop(messages, server)

			{:send_msg_to_group, group_name, msg} ->
				send server, {:deliver_msg_to_group, self, group_name, msg}
				loop(messages, server)
		end
	end

	def create_group(client, group_name) do
		send client, {:create_group, group_name}
	end

	def join_group(client, group_name) do
		send client, {:join_group, group_name}
	end

	def send_msg(client, dest, msg) do
		send client, {:send_msg, dest, msg}
	end

	def read_msg(client, origin) do
		send client, {:read_msg, origin}
	end	

	def send_msg_to_group(client, group_name, msg) do
		send client, {:send_msg_to_group, group_name, msg}
	end

	def started_writing(client, dest) do
		send client, {:started_writing, dest}
	end

	def silence(client, to_be_silence) do
		send client, {:silence, to_be_silence}
	end

	def unsilence(client, to_be_unsilence) do
		send client, {:unsilence, to_be_unsilence}
	end
end 




